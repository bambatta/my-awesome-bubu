export default (fetch) => ({
    getComments(id,payload) { 
      return fetch(`/comments/products/${id}`,{ params: payload });
    },
    sendComment(id, payload) {
        console.log(payload, id)
      return fetch(`/comments/products/${id}`, { body: payload, method: "post"});
    }

})