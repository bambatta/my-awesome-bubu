export default (fetch) => ({
    profile() {
      return fetch(`/my/profile`);
    },
    profileUpdate(payload) {
        return fetch(`/my/profile`, {body: payload, method: "post"})
    }
    
}) 