import {useAuthStore} from "./auth"
import {useLoginStatus} from "./saveHeadLogin"

export default ()=>{
    return {
        auth:useAuthStore(),
        checkLogin: useLoginStatus()
    }
}
